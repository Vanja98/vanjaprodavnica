$(document).ready(function () {    // Čeka se trenutak kada je DOM(Document Object Model) učitan da bi JS mogao sa njim da manipuliše.


    var id=localStorage.getItem('id');


    $.ajax({
        type: "POST",                                                // HTTP metoda
        url: "http://localhost:8085/api/dostavljaci/dostavi/" + id,                 // URL koji se gađa
        dataType: "json",                                           // tip povratne vrednosti
        success: function (data) {
            console.log("SUCCESS : ", data);                        // ispisujemo u konzoli povratnu vrednost
            for (i = 0; i < data.length; i++) {                     // prolazimo kroz listu svih zaposlenih
                            var row = "<tr>";                                   // kreiramo red za tabelu
                            row += "<td>" + data[i]['id'] + "</td>";
                            row += "<td>" + data[i]['status'] + "</td>";

                            var btn = "<button class='btnDostavi' id = " + data[i]['id'] + ">Dostavljeno</button>";
                            row += "<td>" + btn + "</td>";
                            $('#korpe').append(row);                        // ubacujemo kreirani red u tabelu čiji je id = employees
                        }

        },
               error: function (data) {
                   console.log("ERROR : ", data);
               }
           });
       });

$(document).on('click', '.btnDostavi', function () {    // Čeka se trenutak kada je DOM(Document Object Model) učitan da bi JS mogao sa njim da manipuliše.


    var idD=localStorage.getItem('id');
    var idK=this.id;

    var myJSON=formToJSON(idD,idK);

    $.ajax({
        type: "POST",                                                // HTTP metoda
        url: "http://localhost:8085/api/dostavljaci/dostavljeno",                 // URL koji se gađa
        dataType: "json",
        contentType: "application/json",
        data:myJSON,                                          // tip povratne vrednosti
        success: function (data) {
            console.log("SUCCESS : ", data);
            window.location.href = "dostava.html";// ispisujemo u konzoli povratnu vrednost
            /*if (data==1){
                alert("Uspesno preuzeta korpa");
                window.location.href = "dostavljac.html";
                }
            else{
                alert("Nije moguce preuzeti korpu");
                window.location.href = "dostavljac.html";
            }*/




        },
               error: function (data) {
                   window.location.href = "dostava.html";
                   console.log("ERROR : ", data);
               }
           });
       });

       function formToJSON(idD,idK) {
             return JSON.stringify(
                      {
                        "idD": idD,
                        "idK": idK
                       }
             );
       };