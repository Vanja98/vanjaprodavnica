$(document).on("submit", "form", function (event) {           // kada je submitovana forma za kreiranje novog zaposlenog
    event.preventDefault();

    var namee = $("#namee").val();
    var lastName = $("#lastName").val();
    var positionn = 'Dostavljac';
    var userName = $("#userName").val();
    var password = $("#password").val();
    var adress = $("#adress").val();
    var email = $("#email").val();
    var phoneNumber = $("#phoneNumber").val();

    var newKupacJSON = formToJSON(namee,lastName, userName, password, positionn, adress, email, phoneNumber);  // pozivamo pomoćnu metodu da kreira JSON

    $.ajax({
        type: "POST",                                               // HTTP metoda je POST
        url: "http://localhost:8085/api/dostavljaci/registration",                 // URL na koji se šalju podaci
        dataType: "json",                                           // tip povratne vrednosti
        contentType: "application/json",                            // tip podataka koje šaljemo
        data: newKupacJSON,                                      // Šaljemo novog zaposlenog
        success: function () {
            alert(namee + " " + lastName + " je uspešno kreiran kao " + positionn);
            window.location.href = "korisnici.html";
        },
        error: function (error) {
            alert(error);
        }
    });
});

function formToJSON(namee,lastName, userName, password, uloga, adress, email, phoneNumber) {
    return JSON.stringify(
        {
            "namee": namee,
            "lastName": lastName,
            "userName": userName,
            "password": password,
            "positionn": uloga,
            "adress": adress,
            "email": email,
            "phoneNumber": phoneNumber

        }
    );
};
