/*function login(){

    var email= $("#email").val();
    var password= $("#password").val();

    var loginn=formToJSON(email,password);

    $.ajax({
        type:"POST",
        url: "http://localhost8082/api/korisnici",
        dataType: "json",
        contentType: "application/json",
        data:loginn,
        success: function(data){
            window.location.href= "homepage.html";
         }

        error: function(data){
            alert ("Ne postoji korisnik");
         }




    });


}

function formToJSON(email,password){
    return JSON.stringify(

    {

    "email":email,
    "password":password

    }
    );


}*/

$(document).on("submit", "form", function(event){

    // da se izbegne izvrsavanja pravog submita forme
    event.preventDefault();

    var email = $("#email").val();
    var password = $("#password").val();

    var myJSON = formToJSON(email, password);

    $.ajax({
        type: "POST",
        url: "http://localhost:8085/api/korisnici/login",
        dataType: "json",
        contentType: "application/json",
        data: myJSON,
        success: function (data) {
            // data = ulogovani korisnik koji je vratila metoda iz kontrolera
            // mozemo tu vrednost da ispisemo u konzoli
            console.log(data);

            // postavljamo ulogovanog korisnika na localStorage
            // na isti nacin moze da se postavi i email, username itd.
            localStorage.setItem('positionn', data['positionn']);

            localStorage.setItem('id', data['id']);

            // kasnije u bilo kom js fajlu moze da se dobavi ulogovani korisnik ili njegova uloga na sledeci nacin:
            var ulogaUlogovanogKorisnika = localStorage.getItem('positionn');
            // ispisujemo ulogu u konzoli da bismo potvrdili da je sve u redu
            console.log("Ovo je postavljena uloga ulogovanog korisnika:" + ulogaUlogovanogKorisnika);

            // redirektujemo se na neku drugu stranicu
            window.location.href = "pocetna.html";
        },
        error: function (data) {
            console.log(data);
            alert("Greska");
        }
    });
});

function formToJSON(email, password) {
    return JSON.stringify(
        {
            "email": email,
            "password": password
        }
    );
};//ne treba mozda tacka zarez


$(document).ready(function () {    // Čeka se trenutak kada je DOM(Document Object Model) učitan da bi JS mogao sa njim da manipuliše.
    // ajax poziv
    $.ajax({
        type: "GET",                                                // HTTP metoda
        url: "http://localhost:8085/api/korisnici",                 // URL koji se gađa
        dataType: "json",                                           // tip povratne vrednosti
        success: function (data) {
            console.log("SUCCESS : ", data);                        // ispisujemo u konzoli povratnu vrednost
            for (i = 0; i < data.length; i++) {                     // prolazimo kroz listu svih zaposlenih
                var row = "<tr>";                                   // kreiramo red za tabelu
                row += "<td>" + data[i]['id'] + "</td>";     // ubacujemo podatke jednog zaposlenog u polja
                row += "<td>" + data[i]['namee'] + "</td>";
                row += "<td>" + data[i]['lastName'] + "</td>";
                row += "<td>" + data[i]['userName'] + "</td>";
                row += "<td>" + data[i]['password'] + "</td>";
                row += "<td>" + data[i]['positionn'] + "</td>";
                row += "<td>" + data[i]['adress'] + "</td>";
                row += "<td>" + data[i]['email'] + "</td>";
                row += "<td>" + data[i]['phoneNumber'] + "</td>";

                // kreiramo button i stavljamo idButton = idZaposlenog
                var btn = "<button class='btnSeeMore' id = " + data[i]['id'] + ">See more</button>";
                var btn1="<button class='btnDelete' id= " + data[i]['id'] + "> Delete </button>";
                var btn2="<button class='btnPromeniUlogu' id= " + data[i]['id'] + "> Promeni Ulogu </button>";
                var btn3="<button class='btnPromeniUlogu' id= " + data[i]['id'] + "> Izmeni Korisnika </button>";

                row += "<td>" + btn + "</td>";                      // ubacujemo button u poslednje polje reda
                row += "<td>" + btn1 + "</td>";
                row += "<td>" + btn2 + "</td>";
                row += "<td>" + btn3 + "</td>";

                $('#korisnici').append(row);                        // ubacujemo kreirani red u tabelu čiji je id = employees
            }
            var ulogaUlogovanogKorisnika = localStorage.getItem('positionn');
            if (ulogaUlogovanogKorisnika!="Administrator"){
                var provera=$("#allKorisnici");
                provera.hide();
                }

        },
               error: function (data) {
                   console.log("ERROR : ", data);
               }
           });
       });

$(document).on('click', '.btnSeeMore', function () {            // kada je button (čija je class = btnSeeMore) kliknut
    var korisniciDiv = $("#allKorisnici");                      // dobavi element čiji je id = allEmployees  (pogledati html)
    korisniciDiv.hide();                                        // sakrij taj element

    // nakon što korisnik klikne button See more možemo i samo da se prebacimo na employee.html
    // tada ajax poziv za dobavljanje jednog zaposlenog moze da bude u fajlu employee.js
    $.ajax({
        type: "GET",
        url: "http://localhost:8085/api/korisnici/" + this.id,  // this.id je button id, a kao button id je postavljen id zaposlenog
        dataType: "json",
        success: function (data) {
            console.log("SUCCESS : ", data);
            $('#firstName').append(data['namee']);
            $('#lastName').append(data['lastName']);
            $('#position').append(data['positionn']);
            var employeesDiv = $("#oneKorisnik");              // dobavi element čiji je id = oneEmployee
            employeesDiv.show();                               // prikaži taj element
        },
        error: function (data) {
            console.log("ERROR : ", data);
        }
    });
});

$(document).on('click', '.btnDelete', function () {            // kada je button (čija je class = btnSeeMore) kliknut
                                            // sakrij taj element

    // nakon što korisnik klikne button See more možemo i samo da se prebacimo na employee.html
    // tada ajax poziv za dobavljanje jednog zaposlenog moze da bude u fajlu employee.js
    $.ajax({
        type: "DELETE",
        url: "http://localhost:8085/api/korisnici/delete/" + this.id,  // this.id je button id, a kao button id je postavljen id zaposlenog
        dataType: "json",
        success: function (data) {
            window.location.href = "delete.html";                             // prikaži taj element
        },
        error: function () {
            console.log("Korisnik je obrisan");
            window.location.href = "korisnici.html";
        }
    });
});

$(document).on('click', '.btnPromeniUlogu', function () {            // kada je button (čija je class = btnSeeMore) kliknut
                                            // sakrij taj element

    // nakon što korisnik klikne button See more možemo i samo da se prebacimo na employee.html
    // tada ajax poziv za dobavljanje jednog zaposlenog moze da bude u fajlu employee.js
    $.ajax({
        type: "GET",
        url: "http://localhost:8085/api/korisnici/promeni/" + this.id,  // this.id je button id, a kao button id je postavljen id zaposlenog
        dataType: "json",
        success: function (data) {
            window.location.href = "korisnici.html";                             // prikaži taj element
        },
        error: function () {
            console.log("ne radi");
            window.location.href = "korisnici.html";
        }
    });
});