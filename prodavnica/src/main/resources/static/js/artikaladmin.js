$(document).ready(function () {    // Čeka se trenutak kada je DOM(Document Object Model) učitan da bi JS mogao sa njim da manipuliše.
    // ajax poziv
    $.ajax({
        type: "GET",                                                // HTTP metoda
        url: "http://localhost:8085/api/artikli",                 // URL koji se gađa
        dataType: "json",                                           // tip povratne vrednosti
        success: function (data) {
            console.log("SUCCESS : ", data);                        // ispisujemo u konzoli povratnu vrednost
            for (i = 0; i < data.length; i++) {                     // prolazimo kroz listu svih zaposlenih
                var row = "<tr>";                                   // kreiramo red za tabelu
                row += "<td>" + data[i]['id'] + "</td>";     // ubacujemo podatke jednog zaposlenog u polja
                row += "<td>" + data[i]['itemName'] + "</td>";
                row += "<td>" + data[i]['itemDescription'] + "</td>";
                row += "<td>" + data[i]['itemPrice'] + "</td>";
                row += "<td>" + data[i]['quantity'] + "</td>";
                row += "<td>" + data[i]['categoryy'] + "</td>";

                var btn = "<button class='btnDelete' id = " + data[i]['id'] + ">Delete</button>";
                row += "<td>" + btn + "</td>";
                $('#artikli').append(row);                        // ubacujemo kreirani red u tabelu čiji je id = employees
            }
           },

        error: function (data) {
            console.log("ERROR : ", data);
        }
    });
});


$(document).on('click', '.btnDelete', function () {            // kada je button (čija je class = btnSeeMore) kliknut
                                            // sakrij taj element

    // nakon što korisnik klikne button See more možemo i samo da se prebacimo na employee.html
    // tada ajax poziv za dobavljanje jednog zaposlenog moze da bude u fajlu employee.js
    $.ajax({
        type: "DELETE",
        url: "http://localhost:8085/api/artikli/delete/" + this.id,  // this.id je button id, a kao button id je postavljen id zaposlenog
        dataType: "json",
        success: function (data) {
            window.location.href = "delete.html";                             // prikaži taj element
        },
        error: function () {
            console.log("Korisnik je obrisan");
            window.location.href = "artikliAdmin.html";
        }
    });
});