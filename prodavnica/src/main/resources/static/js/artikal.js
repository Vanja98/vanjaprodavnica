$(document).ready(function () {    // Čeka se trenutak kada je DOM(Document Object Model) učitan da bi JS mogao sa njim da manipuliše.
    // ajax poziv
    $.ajax({
        type: "GET",                                                // HTTP metoda
        url: "http://localhost:8085/api/artikli",                 // URL koji se gađa
        dataType: "json",                                           // tip povratne vrednosti
        success: function (data) {
            console.log("SUCCESS : ", data);                        // ispisujemo u konzoli povratnu vrednost
            for (i = 0; i < data.length; i++) {                     // prolazimo kroz listu svih zaposlenih
                var row = "<tr>";                                   // kreiramo red za tabelu
                row += "<td>" + data[i]['id'] + "</td>";     // ubacujemo podatke jednog zaposlenog u polja
                row += "<td>" + data[i]['itemName'] + "</td>";
                row += "<td>" + data[i]['itemDescription'] + "</td>";
                row += "<td>" + data[i]['itemPrice'] + "</td>";
                row += "<td>" + data[i]['quantity'] + "</td>";
                row += "<td>" + data[i]['categoryy'] + "</td>";

                var btn = "<button class='btnKupi' id = " + data[i]['id'] + ">Stavi u korpu</button>";
                var btn1 = "<button class='btnOmiljen' id = " + data[i]['id'] + ">Omiljen</button>";
                row += "<td>" + btn + "</td>";
                row += "<td>" + btn1 + "</td>";

                $('#artikli').append(row);                        // ubacujemo kreirani red u tabelu čiji je id = employees
            }
           },

        error: function (data) {
            console.log("ERROR : ", data);
        }
    });
});