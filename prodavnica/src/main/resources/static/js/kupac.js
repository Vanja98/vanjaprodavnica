$(document).on('click', '.Pregled', function () {            // kada je button (čija je class = btnSeeMore) kliknut
    var korisniciDiv = $("#Kupac");                      // dobavi element čiji je id = allEmployees  (pogledati html)
    korisniciDiv.hide();                                        // sakrij taj element

    var idk=localStorage.getItem('id');
    // nakon što korisnik klikne button See more možemo i samo da se prebacimo na employee.html
    // tada ajax poziv za dobavljanje jednog zaposlenog moze da bude u fajlu employee.js
    $.ajax({
        type: "GET",
        url: "http://localhost:8085/api/korisnici/" + idk,  // this.id je button id, a kao button id je postavljen id zaposlenog
        dataType: "json",
        success: function (data) {
            console.log("SUCCESS : ", data);
            $('#firstName').append(data['namee']);
            $('#lastName').append(data['lastName']);
            $('#position').append(data['positionn']);
            var employeesDiv = $("#oneKorisnik");              // dobavi element čiji je id = oneEmployee
            employeesDiv.show();                               // prikaži taj element
        },
        error: function (data) {
            console.log("ERROR : ", data);
        }
    });
});