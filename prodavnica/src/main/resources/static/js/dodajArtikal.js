$(document).on("submit", "form", function (event) {           // kada je submitovana forma za kreiranje novog zaposlenog
    event.preventDefault();

    var itemName = $("#itemName").val();
    var itemDescription = $("#itemDescription").val();
    var itemPrice = $("#itemPrice").val();
    var quantity = $("#quantity").val();
    var categoryy = $("#categoryy").val();

    var newKupacJSON = formToJSON(itemName, itemDescription,itemPrice,quantity,categoryy);  // pozivamo pomoćnu metodu da kreira JSON

    $.ajax({
        type: "POST",                                               // HTTP metoda je POST
        url: "http://localhost:8085/api/artikli/registration",                 // URL na koji se šalju podaci
        dataType: "json",                                           // tip povratne vrednosti
        contentType: "application/json",                            // tip podataka koje šaljemo
        data: newKupacJSON,                                      // Šaljemo novog zaposlenog
        success: function () {
            alert(itemName + " knjiga je uspešno kreirana ");
            window.location.href = "artikliAdmin.html";
        },
        error: function (error) {
            alert(error);
        }
    });
});

function formToJSON(itemName,itemDescription,itemPrice,quantity,categoryy) {
    return JSON.stringify(
        {
            "itemName": itemName,
            "itemDescription": itemDescription,
            "itemPrice": itemPrice,
            "quantity": quantity,
            "categoryy": categoryy

        }
    );
};
