package ftn.projekat.VanjaProdavnica.repository;

import ftn.projekat.VanjaProdavnica.entity.Artikal;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ArtikalRepository extends JpaRepository<Artikal, Long> {



}
