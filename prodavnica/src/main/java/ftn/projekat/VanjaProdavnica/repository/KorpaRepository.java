package ftn.projekat.VanjaProdavnica.repository;

import ftn.projekat.VanjaProdavnica.entity.Korpa;
import ftn.projekat.VanjaProdavnica.entity.Kupac;
import ftn.projekat.VanjaProdavnica.entity.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface KorpaRepository extends JpaRepository<Korpa, Long> {
    List<Korpa> findByBuyerAndStatus(Kupac kupac, Status status);
    List<Korpa> findAllByStatusAndDateeGreaterThan(Status status, Date date);
}
