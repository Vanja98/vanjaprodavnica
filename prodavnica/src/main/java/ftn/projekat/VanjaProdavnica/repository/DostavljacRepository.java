package ftn.projekat.VanjaProdavnica.repository;

import ftn.projekat.VanjaProdavnica.entity.Dostavljac;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DostavljacRepository extends JpaRepository<Dostavljac, Long> {
}
