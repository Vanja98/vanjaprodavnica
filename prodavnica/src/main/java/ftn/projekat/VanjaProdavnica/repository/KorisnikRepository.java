package ftn.projekat.VanjaProdavnica.repository;

import ftn.projekat.VanjaProdavnica.entity.Korisnik;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KorisnikRepository extends JpaRepository<Korisnik, Long> {
    Korisnik findByEmailAndPassword(String email, String password);
    Korisnik findOneByEmail(String email);
}
