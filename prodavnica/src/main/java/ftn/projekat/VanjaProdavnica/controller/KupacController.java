package ftn.projekat.VanjaProdavnica.controller;


import ftn.projekat.VanjaProdavnica.entity.Korpa;
import ftn.projekat.VanjaProdavnica.entity.Kupac;
import ftn.projekat.VanjaProdavnica.entity.dto.KorpaDTO;
import ftn.projekat.VanjaProdavnica.entity.dto.KorpaKupacDTO;
import ftn.projekat.VanjaProdavnica.entity.dto.KupacDTO;
import ftn.projekat.VanjaProdavnica.entity.dto.PrethodnaKorpaDTO;
import ftn.projekat.VanjaProdavnica.service.KorisnikService;
import ftn.projekat.VanjaProdavnica.service.KupacService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value="/api/kupci")
public class KupacController {
    @Autowired
    private KupacService kupacService;


    @Autowired
    public KupacController(KupacService kupacService) {
        this.kupacService = kupacService;
    }


    @PostMapping(
            value="/registration",
            consumes = MediaType.APPLICATION_JSON_VALUE,     // tip podataka koje metoda može da primi
            produces = MediaType.APPLICATION_JSON_VALUE)     // tip odgovora
    public ResponseEntity<KorpaKupacDTO> createKupac(@RequestBody KorpaKupacDTO kkupacDTO) throws Exception {



        Kupac kupac = new Kupac(kkupacDTO.getNamee(),kkupacDTO.getLastName(),kkupacDTO.getUserName(),kkupacDTO.getPassword(),kkupacDTO.getPositionn(),kkupacDTO.getAdress(),kkupacDTO.getEmail(),kkupacDTO.getPhoneNumber());


        Kupac newKupac = kupacService.create(kupac);


        KorpaKupacDTO newkKupacDTO = new KorpaKupacDTO(newKupac.getId(), newKupac.getNamee(), newKupac.getLastName(), newKupac.getUserName(), newKupac.getPassword(), newKupac.getPositionn(), newKupac.getAdress(), newKupac.getEmail(), newKupac.getPhoneNumber(), kkupacDTO.getDate(),kkupacDTO.getStatus());

        return new ResponseEntity<>(newkKupacDTO, HttpStatus.OK);


    }

    @GetMapping(

            value = "/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE)     // tip odgovora
    public ResponseEntity<List<PrethodnaKorpaDTO>> prethodnekupovine(@PathVariable(name = "id") Long id) throws Exception {

        List<Korpa> korpe=kupacService.prethodni(id);
        List<PrethodnaKorpaDTO> prethodnaKorpaDTOS= new ArrayList<>();
        for(Korpa k: korpe){
            PrethodnaKorpaDTO prethodnaDTO= new PrethodnaKorpaDTO(k.getId());
            prethodnaKorpaDTOS.add(prethodnaDTO);


        }

        return new ResponseEntity<>(prethodnaKorpaDTOS, HttpStatus.OK);



    }


}
