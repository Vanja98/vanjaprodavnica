package ftn.projekat.VanjaProdavnica.controller;

import ftn.projekat.VanjaProdavnica.entity.Dostavljac;
import ftn.projekat.VanjaProdavnica.entity.Korpa;
import ftn.projekat.VanjaProdavnica.entity.Kupac;
import ftn.projekat.VanjaProdavnica.entity.Status;
import ftn.projekat.VanjaProdavnica.entity.dto.IdKorpaDTO;
import ftn.projekat.VanjaProdavnica.entity.dto.KorisnikDTO;
import ftn.projekat.VanjaProdavnica.entity.dto.KorpaDostavljacDTO;
import ftn.projekat.VanjaProdavnica.entity.dto.StatusDTO;
import ftn.projekat.VanjaProdavnica.service.DostavljacService;
import ftn.projekat.VanjaProdavnica.service.KupacService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value="/api/dostavljaci")
public class DostavljacController {

    @Autowired
    private DostavljacService dostavljacService;

    @Autowired
    private KupacService kupacService;


    @Autowired
    public DostavljacController(DostavljacService dostavljacService, KupacService kupacService) {
        this.dostavljacService = dostavljacService;
        this.kupacService=kupacService;
    }

    @PostMapping(
            value="/registration",
            consumes = MediaType.APPLICATION_JSON_VALUE,     // tip podataka koje metoda može da primi
            produces = MediaType.APPLICATION_JSON_VALUE)     // tip odgovora
    public ResponseEntity<KorisnikDTO> createKorisnik(@RequestBody KorisnikDTO korisnikDTO) throws Exception {

        Dostavljac dostavljac = new Dostavljac(korisnikDTO.getNamee(), korisnikDTO.getLastName(), korisnikDTO.getUserName(), korisnikDTO.getPassword(), korisnikDTO.getPositionn(), korisnikDTO.getAdress(), korisnikDTO.getEmail(), korisnikDTO.getPhoneNumber());


        Dostavljac newDostavljac = dostavljacService.create(dostavljac);


        KorisnikDTO newDostavljacDTO = new KorisnikDTO(dostavljac.getId(), dostavljac.getNamee(), dostavljac.getLastName(), dostavljac.getUserName(), dostavljac.getPassword(), dostavljac.getPositionn(), dostavljac.getAdress(), dostavljac.getEmail(), dostavljac.getPhoneNumber());

        return new ResponseEntity<>(newDostavljacDTO, HttpStatus.OK);


    }

    @GetMapping(
            value="/{id}",
            produces=MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<IdKorpaDTO>> dobaviKorpe (@PathVariable(name = "id") Long id) throws Exception{
        List<Kupac> kupci=kupacService.findAll();
        List<IdKorpaDTO> idKorpaDTOS=new ArrayList<>();

        for(Kupac k: kupci){
            List<Korpa> korpe=k.getPreviousItems();
            for(Korpa ko: korpe){
                IdKorpaDTO idKorpaDTO=new IdKorpaDTO(ko.getId(),ko.getStatus(),k.getId());
                idKorpaDTOS.add(idKorpaDTO);
            }

        }

        return new ResponseEntity<>(idKorpaDTOS, HttpStatus.OK);

    }

    @PostMapping(
            value="/preuzmi",
            consumes = MediaType.APPLICATION_JSON_VALUE,     // tip podataka koje metoda može da primi
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<StatusDTO> preuzmiKorpu(@RequestBody KorpaDostavljacDTO korpaDostavljacDTO) throws Exception{

        Long a= korpaDostavljacDTO.getIdD();
        Long b= korpaDostavljacDTO.getIdK();
        Status s= dostavljacService.menjajStatus(a,b);
        StatusDTO statusDTO=new StatusDTO(s);

        return new ResponseEntity<>(statusDTO, HttpStatus.OK);

    }

    @PostMapping(
            value="/dostavi/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<List<IdKorpaDTO>> dobaviKorpe1 (@PathVariable(name = "id") Long id) throws Exception{
        Dostavljac dostavljac=dostavljacService.findOne(id);
        List<Korpa> korpe= dostavljac.getBaskets();
        List<IdKorpaDTO> idKorpaDTOS= new ArrayList<>();

        for(Korpa k: korpe){
            IdKorpaDTO idKorpaDTO=new IdKorpaDTO(k.getId(),k.getStatus());
            idKorpaDTOS.add(idKorpaDTO);

        }

        return new ResponseEntity<>(idKorpaDTOS, HttpStatus.OK);



    }

    @PostMapping(
            value="/dostavljeno",
            consumes = MediaType.APPLICATION_JSON_VALUE,     // tip podataka koje metoda može da primi
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public void dostavaa(@RequestBody KorpaDostavljacDTO korpaDostavljacDTO) throws Exception{

        Long idD=korpaDostavljacDTO.getIdD();

        Long idK=korpaDostavljacDTO.getIdK();

        dostavljacService.brisiKorpu(idD,idK);



    }


}
