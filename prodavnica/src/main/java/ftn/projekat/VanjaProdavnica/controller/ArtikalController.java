package ftn.projekat.VanjaProdavnica.controller;

import ftn.projekat.VanjaProdavnica.entity.*;
import ftn.projekat.VanjaProdavnica.entity.dto.ArtikalDTO;
import ftn.projekat.VanjaProdavnica.entity.dto.ArtikalKupacDTO;
import ftn.projekat.VanjaProdavnica.entity.dto.KorisnikDTO;
import ftn.projekat.VanjaProdavnica.entity.dto.OmiljeniDTO;
import ftn.projekat.VanjaProdavnica.service.KorisnikService;
import ftn.projekat.VanjaProdavnica.service.KorpaService;
import ftn.projekat.VanjaProdavnica.service.KupacService;
import javafx.scene.media.Media;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ftn.projekat.VanjaProdavnica.service.ArtikalService;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value="/api/artikli")
public class ArtikalController {

    @Autowired
    private ArtikalService artikalService;

    @Autowired
    private KorpaService korpaService;

    @Autowired
    private KupacService kupacService;

    @Autowired
    private KorisnikService korisnikService;


    @Autowired
    public ArtikalController(ArtikalService artikalService, KorpaService korpaService, KupacService kupacService, KorisnikService korisnikService) {
        this.artikalService = artikalService;
        this.kupacService = kupacService;
        this.korpaService=korpaService;
        this.korisnikService=korisnikService;
    }



    @GetMapping(                                               // value nije naveden, jer koristimo bazni url
            produces = MediaType.APPLICATION_JSON_VALUE)       // tip odgovora
    public ResponseEntity<List<ArtikalDTO>> getArtikli() {
        // Dobavljamo sve zaposlene
        List<Artikal> artikalList = this.artikalService.findAll();

        // Kreiramo listu DTO objekata
        List<ArtikalDTO> artikalDTOS = new ArrayList<>();

        for (Artikal artikal : artikalList) {
            // Kreiramo EmployeeDTO za svakog zaposlenog, kojeg je vratila metoda findAll() i ubacujemo ga u listu
            ArtikalDTO artikalDTO = new  ArtikalDTO(artikal.getId(), artikal.getItemName(), artikal.getItemDescription(), artikal.getItemPrice(), artikal.getQuantity(), artikal.getCategoryy());
            artikalDTOS.add(artikalDTO);
        }
        return new ResponseEntity<>(artikalDTOS, HttpStatus.OK);
    }


    @DeleteMapping(
            value = "/delete/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE)  // tip odgovora
    public ResponseEntity<ArtikalDTO> deleteKorisnik(@PathVariable(name = "id") Long id) {

        this.artikalService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping(
            value="/registration",
            consumes = MediaType.APPLICATION_JSON_VALUE,     // tip podataka koje metoda može da primi
            produces = MediaType.APPLICATION_JSON_VALUE)     // tip odgovora
    public ResponseEntity<ArtikalDTO> createArtikal(@RequestBody ArtikalDTO artikalDTO) throws Exception {

        Artikal artikal = new Artikal(artikalDTO.getItemName(),artikalDTO.getItemDescription(),artikalDTO.getItemPrice(),artikalDTO.getQuantity(),artikalDTO.getCategoryy());


        Artikal newArtikal = artikalService.create(artikal);


        ArtikalDTO newArtikalDTO = new ArtikalDTO(newArtikal.getId(),newArtikal.getItemName(),newArtikal.getItemDescription(),newArtikal.getItemPrice(),newArtikal.getQuantity(),newArtikal.getCategoryy());

        return new ResponseEntity<>(newArtikalDTO, HttpStatus.OK);


    }

    @PostMapping(
            value="/dodaj",
            consumes= MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ArtikalKupacDTO> dodajArtikal(@RequestBody ArtikalKupacDTO artikalKupacDTO) throws Exception{

        Long a =artikalKupacDTO.getIdK();

        Status status=artikalKupacDTO.getStatus();

        Long b =artikalKupacDTO.getIdA();

        Artikal artikal=artikalService.findOne(b);

        List<Korpa>  korpe=korpaService.findOne(a,status,artikal);

        return new ResponseEntity<>(artikalKupacDTO, HttpStatus.OK);

    }

    @PostMapping(
            value="/dodaj/omiljeni",
            consumes= MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ArtikalKupacDTO> dodajOmiljen(@RequestBody ArtikalKupacDTO artikalKupacDTO) throws Exception{

        Long a =artikalKupacDTO.getIdK();

        Long b =artikalKupacDTO.getIdA();

        Artikal artikal=artikalService.findOne(b);

        Kupac kupac=kupacService.dodajOmiljeni(a,artikal);

        return new ResponseEntity<>(artikalKupacDTO, HttpStatus.OK);

    }

    @GetMapping(
            value = "/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE)       // tip odgovora
    public ResponseEntity<List<ArtikalDTO>> pregledOmiljenih(@PathVariable(name = "id") Long id) {

        Kupac kupac = this.kupacService.findOne(id);

        List<ArtikalDTO> artikliDTOS= new ArrayList<>();

        List<Artikal> omiljeni=  kupac.getFavItems();

        for (Artikal artikal : omiljeni) {
            // Kreiramo EmployeeDTO za svakog zaposlenog, kojeg je vratila metoda findAll() i ubacujemo ga u listu
            ArtikalDTO artikalDTO = new  ArtikalDTO(artikal.getId(),artikal.getItemName(),artikal.getItemDescription(),artikal.getItemPrice(),artikal.getQuantity(),artikal.getCategoryy());
            artikliDTOS.add(artikalDTO);
        }
        return new ResponseEntity<>(artikliDTOS, HttpStatus.OK);
    }








}
