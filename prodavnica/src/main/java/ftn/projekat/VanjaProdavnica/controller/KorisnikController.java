package ftn.projekat.VanjaProdavnica.controller;

import ftn.projekat.VanjaProdavnica.entity.Korisnik;
import ftn.projekat.VanjaProdavnica.entity.dto.KorisnikDTO;
import ftn.projekat.VanjaProdavnica.entity.dto.LoginKorisnikDTO;
import ftn.projekat.VanjaProdavnica.service.KorisnikService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping(value="/api/korisnici")
public class KorisnikController {

    @Autowired
    private KorisnikService korisnikService;

    /*private final KorisnikService korisnikService;*/
    @Autowired
    public KorisnikController(KorisnikService korisnikService) {
        this.korisnikService = korisnikService;
    }




    @PostMapping(
            value="/login",
            consumes= MediaType.APPLICATION_JSON_VALUE,
            produces= MediaType.APPLICATION_JSON_VALUE
    )

    public ResponseEntity<KorisnikDTO> login(@RequestBody LoginKorisnikDTO korisnikDTO) throws Exception{

        Korisnik korisnik = korisnikService.login(korisnikDTO.getEmail(), korisnikDTO.getPassword());

        KorisnikDTO logovaniKorisnik = new KorisnikDTO();

        if (korisnik != null) {

            logovaniKorisnik = new KorisnikDTO(korisnik.getId(), korisnik.getNamee(), korisnik.getLastName(), korisnik.getUserName(), korisnik.getPassword(), korisnik.getPositionn(), korisnik.getAdress(), korisnik.getEmail(), korisnik.getPhoneNumber());
            return new ResponseEntity<>(logovaniKorisnik, HttpStatus.OK);

        }
        return new ResponseEntity<>(logovaniKorisnik, HttpStatus.BAD_REQUEST);

    }

    @GetMapping(
            value = "/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE)  // tip odgovora
    public ResponseEntity<KorisnikDTO> pregledKorisnika(@PathVariable(name = "id") Long id) {
        // Dobavljamo zaposlenog po ID-u
        Korisnik korisnik = this.korisnikService.findOne(id);

        // Kreiramo objekat klase EmployeeDTO
        KorisnikDTO korisnikDTO = new KorisnikDTO(korisnik.getId(), korisnik.getNamee(), korisnik.getLastName(), korisnik.getUserName(), korisnik.getPassword(), korisnik.getPositionn(), korisnik.getAdress(), korisnik.getEmail(), korisnik.getPhoneNumber());


        // Vraćamo ResponseEntity
        // ResponseEntity predstavlja ceo HTTP odgovor: status kod, zaglavlja i body.
        // Možemo u potpunosti da kontrolišemo šta će se nalaziti u odgovoru koji želimo da vratimo.
        // Ako je došlo do greške postavljamo drugi HttpStatus npr. HttpStatus.BAD_REQUEST
        return new ResponseEntity<>(korisnikDTO, HttpStatus.OK);
    }

    @GetMapping(                                               // value nije naveden, jer koristimo bazni url
            produces = MediaType.APPLICATION_JSON_VALUE)       // tip odgovora
    public ResponseEntity<List<KorisnikDTO>> getKorisnici() {
        // Dobavljamo sve zaposlene
        List<Korisnik> korisnikList = this.korisnikService.findAll();

        // Kreiramo listu DTO objekata
        List<KorisnikDTO> korisnikDTOS = new ArrayList<>();

        for (Korisnik korisnik : korisnikList) {
            // Kreiramo EmployeeDTO za svakog zaposlenog, kojeg je vratila metoda findAll() i ubacujemo ga u listu
            KorisnikDTO korisnikDTO = new  KorisnikDTO(korisnik.getId(), korisnik.getNamee(), korisnik.getLastName(), korisnik.getUserName(), korisnik.getPassword(), korisnik.getPositionn(), korisnik.getAdress(), korisnik.getEmail(), korisnik.getPhoneNumber());
            korisnikDTOS.add(korisnikDTO);
        }
        return new ResponseEntity<>(korisnikDTOS, HttpStatus.OK);
    }

   /* @PostMapping(
            value="/registration",
            consumes = MediaType.APPLICATION_JSON_VALUE,     // tip podataka koje metoda može da primi
            produces = MediaType.APPLICATION_JSON_VALUE)     // tip odgovora
    public ResponseEntity<KorisnikDTO> createKorisnik(@RequestBody KorisnikDTO korisnikDTO) throws Exception {

        Korisnik korisnik = new Korisnik(korisnikDTO.getId(), korisnikDTO.getNamee(), korisnikDTO.getLastName(), korisnikDTO.getUserName(), korisnikDTO.getPassword(), korisnikDTO.getPositionn(), korisnikDTO.getAdress(), korisnikDTO.getEmail(), korisnikDTO.getPhoneNumber());


        Korisnik newKorisnik = korisnikService.create(korisnik);


        KorisnikDTO newKorisnikDTO = new KorisnikDTO(korisnik.getId(), korisnik.getNamee(), korisnik.getLastName(), korisnik.getUserName(), korisnik.getPassword(), korisnik.getPositionn(), korisnik.getAdress(), korisnik.getEmail(), korisnik.getPhoneNumber());

        return new ResponseEntity<>(newKorisnikDTO, HttpStatus.OK);


    }*/

    @DeleteMapping(
            value = "/delete/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE)  // tip odgovora
    public ResponseEntity<KorisnikDTO> deleteKorisnik(@PathVariable(name = "id") Long id) {

        this.korisnikService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping(
            value = "/promeni/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE)  // tip odgovora
            public ResponseEntity<KorisnikDTO> promeniUlogu(@PathVariable(name = "id") Long id) {

        this.korisnikService.change(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }





    @GetMapping("/promena/{id}")
    public String promeniKorisnika(@PathVariable("id") Long id, Model model){
        this.korisnikService.change(id);
        model.addAttribute("korisnici", korisnikService.findAll());
        return "redirect:/korisnici";
    }




}
