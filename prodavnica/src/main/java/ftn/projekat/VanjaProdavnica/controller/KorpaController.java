package ftn.projekat.VanjaProdavnica.controller;



import ftn.projekat.VanjaProdavnica.entity.Artikal;
import ftn.projekat.VanjaProdavnica.entity.Korpa;
import ftn.projekat.VanjaProdavnica.entity.Status;
import ftn.projekat.VanjaProdavnica.entity.dto.ArtikalDTO;
import ftn.projekat.VanjaProdavnica.entity.dto.IzvestajDTO;
import ftn.projekat.VanjaProdavnica.entity.dto.KorpaDTO;
import ftn.projekat.VanjaProdavnica.service.KorpaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(value="/api/korpa")
public class KorpaController {
    @Autowired
    private KorpaService korpaService;


    @Autowired
    public KorpaController(KorpaService korpaService) {
        this.korpaService = korpaService;
    }


    @PostMapping(

            consumes = MediaType.APPLICATION_JSON_VALUE,     // tip podataka koje metoda može da primi
            produces = MediaType.APPLICATION_JSON_VALUE)     // tip odgovora
    public ResponseEntity<KorpaDTO> createKorpa(@RequestBody KorpaDTO korpaDTO) throws Exception {

        Korpa korpa = new Korpa(korpaDTO.getDate());


        Korpa newKorpa = korpaService.create(korpa);


        KorpaDTO newKorpaDTO = new KorpaDTO();

        return new ResponseEntity<>(newKorpaDTO, HttpStatus.OK);


    }

    @PostMapping(

            value = "/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE)     // tip odgovora
    public void kupovina(@PathVariable(name = "id") Long id) throws Exception {

        korpaService.kupi(id);
    }

    @GetMapping(

            value = "/prikaz/{id}",
            produces = MediaType.APPLICATION_JSON_VALUE)     // tip odgovora
    public ResponseEntity<List<ArtikalDTO>> prethodnekupovine(@PathVariable(name = "id") Long id) throws Exception {

        Korpa korpa=korpaService.findOnee(id);

        List<Artikal> artikli=korpa.getItems();

        List<ArtikalDTO> artikalDTOS= new ArrayList<>();

        for(Artikal a: artikli){
            ArtikalDTO artikalDTO= new ArtikalDTO(a.getId(),a.getItemName(),a.getItemDescription(),a.getItemPrice(),a.getQuantity(),a.getCategoryy());
            artikalDTOS.add(artikalDTO);
        }

        return new ResponseEntity<>(artikalDTOS, HttpStatus.OK);


    }

    @GetMapping(

            value = "/dnevni",
            produces = MediaType.APPLICATION_JSON_VALUE)     // tip odgovora
    public ResponseEntity<List<IzvestajDTO>> prethodnekupovine() throws Exception {

        Date date= new Date();

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, -1); //minus number would decrement the days
        Date datum=cal.getTime();

        List<Korpa> korpe=korpaService.izvestaj(datum);

        List<IzvestajDTO> izvestajDTOS=new ArrayList<>();

        double ponisteni=korpaService.brojPonistenih(datum);

        for(Korpa k:korpe){
            List<Artikal> artikals=k.getItems();
            IzvestajDTO izvestajDTO= new IzvestajDTO();
            izvestajDTO.setKorpe(k.getId());

            for(Artikal a:artikals){
                izvestajDTO.setIznos(a.getItemPrice());
                izvestajDTO.setBroj(ponisteni);
                izvestajDTOS.add(izvestajDTO);

            }



        }


        return new ResponseEntity<>(izvestajDTOS, HttpStatus.OK);


    }
    @GetMapping(

            value = "/nedeljni",
            produces = MediaType.APPLICATION_JSON_VALUE)     // tip odgovora
    public ResponseEntity<List<IzvestajDTO>> prethodnekupovine1() throws Exception {

        Date date= new Date();

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, -7); //minus number would decrement the days
        Date datum=cal.getTime();

        List<Korpa> korpe=korpaService.izvestaj(datum);

        List<IzvestajDTO> izvestajDTOS=new ArrayList<>();

        double ponisteni=korpaService.brojPonistenih(datum);

        for(Korpa k:korpe){
            List<Artikal> artikals=k.getItems();
            IzvestajDTO izvestajDTO= new IzvestajDTO();
            izvestajDTO.setKorpe(k.getId());

            for(Artikal a:artikals){
                izvestajDTO.setIznos(a.getItemPrice());
                izvestajDTO.setBroj(ponisteni);
                izvestajDTOS.add(izvestajDTO);

            }



        }


        return new ResponseEntity<>(izvestajDTOS, HttpStatus.OK);


    }
    @GetMapping(

            value = "/mesecni",
            produces = MediaType.APPLICATION_JSON_VALUE)     // tip odgovora
    public ResponseEntity<List<IzvestajDTO>> prethodnekupovine2() throws Exception {

        Date date= new Date();

        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, -30); //minus number would decrement the days
        Date datum=cal.getTime();

        List<Korpa> korpe=korpaService.izvestaj(datum);

        List<IzvestajDTO> izvestajDTOS=new ArrayList<>();

        double ponisteni=korpaService.brojPonistenih(datum);

        for(Korpa k:korpe){
            List<Artikal> artikals=k.getItems();
            IzvestajDTO izvestajDTO= new IzvestajDTO();
            izvestajDTO.setKorpe(k.getId());

            for(Artikal a:artikals){
                izvestajDTO.setIznos(a.getItemPrice());
                izvestajDTO.setBroj(ponisteni);
                izvestajDTOS.add(izvestajDTO);

            }



        }


        return new ResponseEntity<>(izvestajDTOS, HttpStatus.OK);


    }
}
