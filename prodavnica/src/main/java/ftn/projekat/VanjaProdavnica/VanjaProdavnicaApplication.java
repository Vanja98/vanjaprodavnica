package ftn.projekat.VanjaProdavnica;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VanjaProdavnicaApplication {

	public static void main(String[] args) {
		SpringApplication.run(VanjaProdavnicaApplication.class, args);
	}

}
