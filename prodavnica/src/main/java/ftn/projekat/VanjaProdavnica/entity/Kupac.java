package ftn.projekat.VanjaProdavnica.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@DiscriminatorValue("kupac")
public class Kupac extends Korisnik {

    @ManyToMany
    @JoinTable(name = "favourite", joinColumns = @JoinColumn(name = "buyer_id",referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "item_id",referencedColumnName = "id"))
    private List<Artikal> favItems = new ArrayList<>();

    @ManyToMany
    @JoinTable(name = "previous", joinColumns = @JoinColumn(name = "buyer_id",referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "item_id",referencedColumnName = "id"))
    private List<Korpa> previousItems = new ArrayList<>();

    @OneToOne(fetch = FetchType.LAZY)
    @JoinTable(name = "basket_id",joinColumns= @JoinColumn(name="buyer_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "basket_id",referencedColumnName = "id"))
    private Korpa buyersBasket;

    public Kupac(String namee, String lastName, String userName, String password, String positionn, String adress, String email, String phoneNumber){
        super(namee, lastName, userName, password, positionn, adress, email, phoneNumber);


    }

    public Kupac() {
    }

    public Kupac(Long id, String namee, String lastName, String userName, String password, String positionn, String adress, String email, String phoneNumber) {
        super(id, namee, lastName, userName, password, positionn, adress, email, phoneNumber);
    }

    public List<Artikal> getFavItems() {
        return favItems;
    }

    public void setFavItems(List<Artikal> favItems) {
        this.favItems = favItems;
    }

    public List<Korpa> getPreviousItems() {
        return previousItems;
    }

    public void setPreviousItems(List<Korpa> previousItems) {
        this.previousItems = previousItems;
    }

    public Korpa getBuyersBasket() {
        return buyersBasket;
    }

    public void setBuyersBasket(Korpa buyersBasket) {
        this.buyersBasket = buyersBasket;
    }
}
