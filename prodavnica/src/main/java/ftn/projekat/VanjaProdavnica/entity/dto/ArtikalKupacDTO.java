package ftn.projekat.VanjaProdavnica.entity.dto;

import ftn.projekat.VanjaProdavnica.entity.Status;

public class ArtikalKupacDTO {

    private Long idA;

    private Long idK;

    private Status status;

    public Long getIdA() {
        return idA;
    }

    public void setIdA(Long idA) {
        this.idA = idA;
    }

    public Long getIdK() {
        return idK;
    }

    public void setIdK(Long idK) {
        this.idK = idK;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public ArtikalKupacDTO(Long idA, Long idK, Status status) {
        this.idA = idA;
        this.idK = idK;
        this.status = status;
    }

    public ArtikalKupacDTO() {
    }
}
