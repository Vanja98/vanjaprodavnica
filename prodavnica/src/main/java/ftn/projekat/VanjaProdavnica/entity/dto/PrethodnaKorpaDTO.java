package ftn.projekat.VanjaProdavnica.entity.dto;

public class PrethodnaKorpaDTO {
    Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PrethodnaKorpaDTO(Long id) {
        this.id = id;
    }

    public PrethodnaKorpaDTO() {
    }
}
