package ftn.projekat.VanjaProdavnica.entity.dto;

public class idDTO {
    Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public idDTO() {
    }

    public idDTO(Long id) {
        this.id = id;
    }
}
