package ftn.projekat.VanjaProdavnica.entity.dto;

import ftn.projekat.VanjaProdavnica.entity.Status;

import java.util.Date;

public class KorpaDTO {
    private Date date;

    private Status status;

    public KorpaDTO() {
    }

    public KorpaDTO(Date date, Status status) {
        this.date = date;
        this.status=status;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
