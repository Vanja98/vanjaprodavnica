package ftn.projekat.VanjaProdavnica.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
public class Artikal implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String itemName;

    @Column
    private String itemDescription;

    @Column
    private double itemPrice;

    @Column
    private int quantity;

    @Column
    private String categoryy;

    @ManyToMany(mappedBy = "favItems")
    private List<Kupac> favCustomers = new ArrayList<>();

    @ManyToMany(mappedBy = "previousItems")
    private Set<Kupac> previousCustomers = new HashSet<>();

    @ManyToMany
    @JoinTable(name = "korpa_artikli",joinColumns= @JoinColumn(name="artikal_id", referencedColumnName = "id"), inverseJoinColumns = @JoinColumn(name = "basket_id",referencedColumnName = "id"))
    private List<Korpa> korpe = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public double getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(double itemPrice) {
        this.itemPrice = itemPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getCategoryy() {
        return categoryy;
    }

    public void setCategoryy(String categoryy) {
        this.categoryy = categoryy;
    }

    public List<Kupac> getFavCustomers() {
        return favCustomers;
    }

    public void setFavCustomers(List<Kupac> favCustomers) {
        this.favCustomers = favCustomers;
    }

    public Set<Kupac> getPreviousCustomers() {
        return previousCustomers;
    }

    public void setPreviousCustomers(Set<Kupac> previousCustomers) {
        this.previousCustomers = previousCustomers;
    }

    public List<Korpa> getItemBasket() {
        return korpe;
    }

    public void setItemBasket(List<Korpa> itemBasket) {
        this.korpe = itemBasket;
    }

    public Artikal(String itemName, String itemDescription, double itemPrice, int quantity, String categoryy) {
        this.itemName = itemName;
        this.itemDescription = itemDescription;
        this.itemPrice = itemPrice;
        this.quantity = quantity;
        this.categoryy = categoryy;
    }

    public Artikal() {
    }
}
