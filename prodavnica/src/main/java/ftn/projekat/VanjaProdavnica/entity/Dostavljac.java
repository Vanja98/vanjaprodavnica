package ftn.projekat.VanjaProdavnica.entity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@DiscriminatorValue("dostavljac")
public class Dostavljac extends Korisnik{

    @OneToMany(mappedBy = "deliverer",cascade = CascadeType.ALL)
    private List<Korpa> baskets = new ArrayList<>();

    public Dostavljac( String namee, String lastName, String userName, String password, String positionn, String adress, String email, String phoneNumber) {
        super(namee, lastName, userName, password, positionn, adress, email, phoneNumber);
    }

    public Dostavljac(){
        super();
    }

    public Long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public  List<Korpa> getBaskets() {
        return baskets;
    }

    public void setBaskets( List<Korpa> baskets) {
        baskets = baskets;
    }
}
