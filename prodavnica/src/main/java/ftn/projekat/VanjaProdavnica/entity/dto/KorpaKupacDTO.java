package ftn.projekat.VanjaProdavnica.entity.dto;

import ftn.projekat.VanjaProdavnica.entity.Status;

import java.util.Date;

public class KorpaKupacDTO {
    private Long id;

    private String namee;

    private String lastName;

    private String userName;

    private String password;

    private String positionn;

    private String adress;

    private String email;

    private String phoneNumber;

    private Date date;

    private Status status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNamee() {
        return namee;
    }

    public void setNamee(String namee) {
        this.namee = namee;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPositionn() {
        return positionn;
    }

    public void setPositionn(String positionn) {
        this.positionn = positionn;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public KorpaKupacDTO() {
    }

    public KorpaKupacDTO(Long id, String namee, String lastName, String userName, String password, String positionn, String adress, String email, String phoneNumber, Date date, Status status) {
        this.id = id;
        this.namee = namee;
        this.lastName = lastName;
        this.userName = userName;
        this.password = password;
        this.positionn = positionn;
        this.adress = adress;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.date = date;
        this.status = status;
    }
}
