package ftn.projekat.VanjaProdavnica.entity.dto;

import ftn.projekat.VanjaProdavnica.entity.Korpa;

import java.util.List;

public class IzvestajDTO {

    Long korpe;

    double broj;

    double iznos;

    public Long getKorpe() {
        return korpe;
    }

    public void setKorpe(Long korpe) {
        this.korpe = korpe;
    }

    public double getBroj() {
        return broj;
    }

    public void setBroj(double broj) {
        this.broj = broj;
    }

    public double getIznos() {
        return iznos;
    }

    public void setIznos(double iznos) {
        this.iznos = iznos;
    }

    public IzvestajDTO(Long korpe, double broj, double iznos) {
        this.korpe = korpe;
        this.broj = broj;
        this.iznos = iznos;
    }

    public IzvestajDTO() {
    }
}
