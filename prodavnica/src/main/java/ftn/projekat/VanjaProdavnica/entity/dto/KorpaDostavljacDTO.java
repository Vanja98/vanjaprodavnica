package ftn.projekat.VanjaProdavnica.entity.dto;

public class KorpaDostavljacDTO {
    Long idD;
    Long idK;

    public Long getIdD() {
        return idD;
    }

    public void setIdD(Long idD) {
        this.idD = idD;
    }

    public Long getIdK() {
        return idK;
    }

    public void setIdK(Long idK) {
        this.idK = idK;
    }

    public KorpaDostavljacDTO() {
    }

    public KorpaDostavljacDTO(Long idD, Long idK) {
        this.idD = idD;
        this.idK = idK;
    }
}
