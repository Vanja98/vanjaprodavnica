package ftn.projekat.VanjaProdavnica.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Time;
import java.util.*;


@Entity
public class Korpa implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column
    private Status status;

    @Column
    private Date datee;

    @ManyToOne
    private Dostavljac deliverer;

    @OneToOne(mappedBy = "buyersBasket", cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = false)
    private Kupac buyer;

    @ManyToMany(mappedBy = "korpe",cascade = CascadeType.ALL)
    private List<Artikal> items = new ArrayList<>();

    public Korpa() {
    }

    public Korpa(Date date) {
        this.datee = date;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ftn.projekat.VanjaProdavnica.entity.Status getStatus() {
        return status;
    }

    public void setStatus(ftn.projekat.VanjaProdavnica.entity.Status statuss) {
        status = statuss;
    }

    public Date getDatee() {
        return datee;
    }

    public void setDatee(Date date) {
        datee = date;
    }

    public Kupac getBuyer() {
        return buyer;
    }

    public void setBuyer(Kupac buyerr) {
        buyer = buyerr;
    }

    public Dostavljac getDeliverer() {
        return deliverer;
    }

    public void setDeliverer(Dostavljac delivererr) {
        deliverer = delivererr;
    }

    public List<Artikal> getItems() {
        return items;
    }

    public void setItems(List<Artikal> itemss) {
        items = itemss;
    }
}
