package ftn.projekat.VanjaProdavnica.entity;

import javax.persistence.*;
import java.io.Serializable;

    @Entity
    @Inheritance(strategy = InheritanceType.SINGLE_TABLE)
    @DiscriminatorColumn(name = "tip")
    public class Korisnik implements Serializable {
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "id")
        protected Long id;

        @Column
        protected String namee;

        @Column
        protected String lastName;

        @Column
        protected String userName;

        @Column
        protected String password;

        @Column
        protected String positionn;

        @Column
        protected String adress;

        @Column
        protected String email;

        @Column
        protected String phoneNumber;

        public Korisnik(Long id, String namee, String lastName, String userName, String password, String positionn, String adress, String email, String phoneNumber) {
            this.id = id;
            this.namee = namee;
            this.lastName = lastName;
            this.userName = userName;
            this.password = password;
            this.positionn = positionn;
            this.adress = adress;
            this.email = email;
            this.phoneNumber = phoneNumber;
        }

        public Korisnik(String namee, String lastName, String userName, String password, String position, String adress, String email, String phoneNumber) {
            this.namee = namee;
            this.lastName = lastName;
            this.userName = userName;
            this.password = password;
            this.positionn = position;
            this.adress = adress;
            this.email = email;
            this.phoneNumber = phoneNumber;
        }

        public Korisnik() {

        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getNamee() {
            return namee;
        }

        public void setNamee(String namee) {
            this.namee = namee;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getPositionn() {
            return positionn;
        }

        public void setPositionn(String positionn) {
            this.positionn = positionn;
        }

        public String getAdress() {
            return adress;
        }

        public void setAdress(String adress) {
            this.adress = adress;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }
    }
