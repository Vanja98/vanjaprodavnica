package ftn.projekat.VanjaProdavnica.entity.dto;

public class ArtikalDTO {

    private Long id;


    private String itemName;


    private String itemDescription;


    private double itemPrice;


    private int quantity;


    private String categoryy;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    public double getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(double itemPrice) {
        this.itemPrice = itemPrice;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getCategoryy() {
        return categoryy;
    }

    public void setCategoryy(String categoryy) {
        this.categoryy = categoryy;
    }

    public ArtikalDTO() {
    }

    public ArtikalDTO(long id, String itemName, String itemDescription, double itemPrice, int quantity, String categoryy) {
        this.id = id;
        this.itemName = itemName;
        this.itemDescription = itemDescription;
        this.itemPrice = itemPrice;
        this.quantity = quantity;
        this.categoryy = categoryy;
    }
}
