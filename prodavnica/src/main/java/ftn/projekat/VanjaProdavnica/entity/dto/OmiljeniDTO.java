package ftn.projekat.VanjaProdavnica.entity.dto;

import ftn.projekat.VanjaProdavnica.entity.Artikal;

public class OmiljeniDTO {

    Artikal artikal;

    public Artikal getArtikal() {
        return artikal;
    }

    public void setArtikal(Artikal artikal) {
        this.artikal = artikal;
    }

    public OmiljeniDTO(Artikal artikal) {
        this.artikal = artikal;
    }

    public OmiljeniDTO() {
    }
}
