package ftn.projekat.VanjaProdavnica.entity.dto;

import ftn.projekat.VanjaProdavnica.entity.Status;

public class IdKorpaDTO {

    Long id;
    Status status;
    Long idK;

    public Long getIdK() {
        return idK;
    }

    public void setIdK(Long idK) {
        this.idK = idK;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public IdKorpaDTO(Long id, Status status,Long idK) {
        this.id = id;
        this.status = status;
        this.idK= idK;
    }

    public IdKorpaDTO(Long id, Status status) {
        this.id = id;
        this.status = status;
    }

    public IdKorpaDTO() {
    }
}
