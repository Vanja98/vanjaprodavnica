package ftn.projekat.VanjaProdavnica.entity.dto;

import ftn.projekat.VanjaProdavnica.entity.Status;

public class StatusDTO {

    Status s;

    public Status getS() {
        return s;
    }

    public void setS(Status s) {
        this.s = s;
    }

    public StatusDTO(Status s) {
        this.s = s;
    }

    public StatusDTO() {
    }
}
