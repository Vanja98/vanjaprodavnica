package ftn.projekat.VanjaProdavnica.service;

import ftn.projekat.VanjaProdavnica.entity.Korisnik;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface KorisnikService {
    Korisnik create(Korisnik korisnik) throws Exception;
    List<Korisnik> findAll();
    void delete(Long id);
    Korisnik findOne(Long id);
    Korisnik login(String email, String password);
    void change(Long id);
   // Boolean proverilogovanje(String korIme, String loz);
}
