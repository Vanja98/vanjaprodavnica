package ftn.projekat.VanjaProdavnica.service;


import ftn.projekat.VanjaProdavnica.entity.Artikal;
import ftn.projekat.VanjaProdavnica.entity.Korpa;
import ftn.projekat.VanjaProdavnica.entity.Kupac;
import ftn.projekat.VanjaProdavnica.entity.Status;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public interface KorpaService {
    Korpa create(Korpa korpa) throws Exception;
    List<Korpa> findOne(Long id, Status status, Artikal artikal);
    void dodaj(Korpa korpa, Artikal artikal);
    void kupi(Long id);
    Korpa findOnee(Long id);
    List<Korpa> izvestaj(Date date);
    double brojPonistenih(Date date);


}

