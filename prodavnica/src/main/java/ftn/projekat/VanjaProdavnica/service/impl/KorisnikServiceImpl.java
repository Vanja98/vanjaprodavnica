package ftn.projekat.VanjaProdavnica.service.impl;

import ftn.projekat.VanjaProdavnica.entity.Korisnik;
import ftn.projekat.VanjaProdavnica.repository.KorisnikRepository;
import ftn.projekat.VanjaProdavnica.service.KorisnikService;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class KorisnikServiceImpl implements KorisnikService {

    private final KorisnikRepository korisnikRepository;

    public KorisnikServiceImpl(KorisnikRepository korisnikRepository) {
        this.korisnikRepository = korisnikRepository;
    }

    /*
        Kreiranje novog zaposlenog i čuvanje u bazi pozivanjem metode save.
        Metoda vraća kreiranog zaposlenog.
    */

    @Override
    public Korisnik create(Korisnik korisnik) throws Exception {
        if (korisnik.getId() != null) {
            throw new Exception("ID must be null!");
        }
        Korisnik newKorisnik = this.korisnikRepository.save(korisnik);
        return newKorisnik;
    }

    @Override
    public List<Korisnik> findAll() {
        List<Korisnik> korisnik = korisnikRepository.findAll();
        return korisnik;
    }

    @Override
    public void delete(Long id) {
        Korisnik korisnik = korisnikRepository.getOne(id);
        korisnikRepository.delete(korisnik);
    }

    @Override
    public Korisnik findOne(Long id) {
        Korisnik korisnik = korisnikRepository.getOne(id);
        return korisnik;
    }

    @Override
    public Korisnik login(String email, String password){
        return  this.korisnikRepository.findByEmailAndPassword(email, password);
    }

    @Override
    public void change(Long id){
        Korisnik korisnik = korisnikRepository.getOne(id);

        if (korisnik.getPositionn().equals("Dostavljac")){
            korisnik.setPositionn("Kupac");
        } else {
            korisnik.setPositionn("Dostavljac");
        }

        korisnikRepository.save(korisnik);
    }


}
