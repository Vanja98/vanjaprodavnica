package ftn.projekat.VanjaProdavnica.service;

import ftn.projekat.VanjaProdavnica.entity.Artikal;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ArtikalService {

    List<Artikal> findAll();
    void delete(Long id);
    Artikal create(Artikal artikal) throws Exception;
    Artikal findOne(Long id);
}

