package ftn.projekat.VanjaProdavnica.service.impl;

import ftn.projekat.VanjaProdavnica.entity.Artikal;
import ftn.projekat.VanjaProdavnica.entity.Korpa;
import ftn.projekat.VanjaProdavnica.entity.Kupac;
import ftn.projekat.VanjaProdavnica.entity.Status;
import ftn.projekat.VanjaProdavnica.repository.ArtikalRepository;
import ftn.projekat.VanjaProdavnica.repository.KorpaRepository;
import ftn.projekat.VanjaProdavnica.repository.KupacRepository;
import ftn.projekat.VanjaProdavnica.service.KorpaService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class KorpaServiceImpl implements KorpaService {
    private final KorpaRepository korpaRepository;

    private final KupacRepository kupacRepository;

    private final ArtikalRepository artikalRepository;

    public KorpaServiceImpl(KorpaRepository korpaRepository,KupacRepository kupacRepository,ArtikalRepository artikalRepository) {
        this.korpaRepository = korpaRepository;
        this.kupacRepository=kupacRepository;
        this.artikalRepository=artikalRepository;
    }

    @Override
    public Korpa create(Korpa korpa) throws Exception {
        if (korpa.getId() != null) {
            throw new Exception("ID must be null!");
        }
        Korpa newKorpa = this.korpaRepository.save(korpa);
        return newKorpa;
    }

    @Override
    public List<Korpa>  findOne(Long id, Status status, Artikal artikal) {
        Kupac kupac=kupacRepository.getOne(id);
        List<Korpa> korpe = korpaRepository.findByBuyerAndStatus(kupac,status);
        List<Korpa> korpe1=artikal.getItemBasket();
        for(Korpa k: korpe){
            korpe1.add(k);
            artikal.setItemBasket(korpe1);
            artikalRepository.save(artikal);
            List<Artikal> artikli=k.getItems();
            artikli.add(artikal);
            k.setItems(artikli);
            Korpa korpica=korpaRepository.save(k);
        }


        return korpe;
    }

    @Override
    public void dodaj (Korpa korpa, Artikal artikal){
       Korpa korpica=korpaRepository.getOne(korpa.getId());
       List<Artikal> artikli=korpa.getItems();
       artikli.add(artikal);
       korpica.setItems(artikli);
       Korpa korpica1=this.korpaRepository.save(korpica);

    }

    @Override
    public void kupi(Long id){
        Status status=null;
        Kupac kupac= kupacRepository.getOne(id);
        List<Korpa> korpe=korpaRepository.findByBuyerAndStatus(kupac,status);

        for(Korpa k: korpe){
            k.setStatus(Status.KUPLJENO);
        }

        Korpa korpica=new Korpa();
        Date datum = new Date();
        korpica.setDatee(datum);
        korpica.setBuyer(kupac);
        Korpa novaKorpa=korpaRepository.save(korpica);
        kupac.setBuyersBasket(novaKorpa);
        Kupac kupac1=kupacRepository.save(kupac);
        List<Korpa> korpee=kupac1.getPreviousItems();

        for(Korpa k:korpe){
            korpee.add(k);


        }

        kupac.setPreviousItems(korpee);

        Kupac kupac2=kupacRepository.save(kupac);


    }

    @Override
    public Korpa findOnee(Long id){
        Korpa korpa=korpaRepository.getOne(id);
        return korpa;
    }

    @Override
    public List<Korpa> izvestaj(Date datum){
        Status status=Status.DOSTAVLJENO;
        List<Korpa> korpe=korpaRepository.findAllByStatusAndDateeGreaterThan(status,datum);
        return korpe;
    }

    @Override
    public double brojPonistenih(Date date){
        Status status=Status.OTKAZANO;
        List<Korpa> korpe=korpaRepository.findAllByStatusAndDateeGreaterThan(status,date);
        double broj=korpe.size();
        return broj;
    }
}

