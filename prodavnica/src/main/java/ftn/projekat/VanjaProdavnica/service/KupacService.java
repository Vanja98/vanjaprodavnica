package ftn.projekat.VanjaProdavnica.service;

import ftn.projekat.VanjaProdavnica.entity.Artikal;
import ftn.projekat.VanjaProdavnica.entity.Korpa;
import ftn.projekat.VanjaProdavnica.entity.Kupac;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface KupacService {
    Kupac create(Kupac kupac) throws Exception;
    Kupac findOne(Long id);
    Kupac dodajOmiljeni(Long id, Artikal artikal);
    List<Kupac> findAll();
    List<Korpa> prethodni(Long id);

}
