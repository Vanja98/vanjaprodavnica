package ftn.projekat.VanjaProdavnica.service;

import ftn.projekat.VanjaProdavnica.entity.Dostavljac;
import ftn.projekat.VanjaProdavnica.entity.Status;
import org.springframework.stereotype.Service;

@Service
public interface DostavljacService {

    Dostavljac create(Dostavljac dostavljac) throws Exception;
    Status menjajStatus(Long idD, Long idK);
    Dostavljac findOne(Long id);
    void brisiKorpu(Long id,Long idk);
}
