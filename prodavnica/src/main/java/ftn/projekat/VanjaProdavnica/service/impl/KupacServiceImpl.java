package ftn.projekat.VanjaProdavnica.service.impl;

import ftn.projekat.VanjaProdavnica.entity.Artikal;
import ftn.projekat.VanjaProdavnica.entity.Korpa;
import ftn.projekat.VanjaProdavnica.entity.Kupac;
import ftn.projekat.VanjaProdavnica.repository.ArtikalRepository;
import ftn.projekat.VanjaProdavnica.repository.KorpaRepository;
import ftn.projekat.VanjaProdavnica.repository.KupacRepository;
import ftn.projekat.VanjaProdavnica.service.KupacService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class KupacServiceImpl implements KupacService {
    private final KupacRepository kupacRepository;

    public KupacServiceImpl(KupacRepository kupacRepository, KorpaRepository korpaRepository, ArtikalRepository artikalRepository) {
        this.kupacRepository = kupacRepository;
        this.korpaRepository = korpaRepository;
        this.artikalRepository= artikalRepository;
    }

    private final KorpaRepository korpaRepository;
    private final ArtikalRepository artikalRepository;


    @Override
    public Kupac create(Kupac kupac) throws Exception {
        if (kupac.getId() != null) {
            throw new Exception("ID must be null!");
        }
        Date datum = new Date();
        Korpa newKorpa = new Korpa(datum);
        Korpa korpica = this.korpaRepository.save(newKorpa);
        kupac.setBuyersBasket(korpica);
        Kupac newKupac = this.kupacRepository.save(kupac);
        return newKupac;
    }

    @Override
    public Kupac findOne(Long id) {
        Kupac korisnik = kupacRepository.getOne(id);
        return korisnik;
    }

    @Override
    public Kupac dodajOmiljeni(Long id, Artikal artikal){
        Kupac kupac=kupacRepository.getOne(id);
        List<Artikal> omiljeni=kupac.getFavItems();
        omiljeni.add(artikal);
        kupac.setFavItems(omiljeni);
        Kupac kupac1=kupacRepository.save(kupac);
        List<Kupac> kupci=artikal.getFavCustomers();
        kupci.add(kupac);
        artikal.setFavCustomers(kupci);
        Artikal artikal1=artikalRepository.save(artikal);
        return kupac;
    }

    @Override
    public List<Kupac> findAll(){
        List<Kupac> kupac = kupacRepository.findAll();
        return kupac;
    }

    @Override
    public List<Korpa> prethodni(Long id){
        Kupac kupac= kupacRepository.getOne(id);
        List<Korpa> korpe = kupac.getPreviousItems();
        return korpe;
    }
}
