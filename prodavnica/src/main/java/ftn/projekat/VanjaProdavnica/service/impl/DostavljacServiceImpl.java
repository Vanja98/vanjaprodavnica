package ftn.projekat.VanjaProdavnica.service.impl;

import ftn.projekat.VanjaProdavnica.entity.Dostavljac;
import ftn.projekat.VanjaProdavnica.entity.Korpa;
import ftn.projekat.VanjaProdavnica.entity.Status;
import ftn.projekat.VanjaProdavnica.repository.DostavljacRepository;
import ftn.projekat.VanjaProdavnica.repository.KorpaRepository;
import ftn.projekat.VanjaProdavnica.service.DostavljacService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DostavljacServiceImpl implements DostavljacService {

    private final DostavljacRepository dostavljacRepository;
    private final KorpaRepository korpaRepository;

    public DostavljacServiceImpl(DostavljacRepository dostavljacRepository,KorpaRepository korpaRepository) {
        this.dostavljacRepository = dostavljacRepository;
        this.korpaRepository=korpaRepository;

    }

    @Override
    public Dostavljac create(Dostavljac dostavljac) throws Exception {
        if (dostavljac.getId() != null) {
            throw new Exception("ID must be null!");
        }

        Dostavljac newDostavljac = this.dostavljacRepository.save(dostavljac);
        return newDostavljac;
    }

    @Override
    public Status menjajStatus(Long idD, Long idK){
        Dostavljac dostavljac=dostavljacRepository.getOne(idD);
        Korpa korpa=korpaRepository.getOne(idK);
        List<Korpa> korpe = dostavljac.getBaskets();
        if ((korpa.getStatus()== Status.KUPLJENO)) {
            korpa.setStatus(Status.DOSTAVAUTOKU);

            korpe.add(korpa);
            dostavljac.setBaskets(korpe);
            Dostavljac dostavljac1 = dostavljacRepository.save(dostavljac);

            korpa.setDeliverer(dostavljac1);

            korpaRepository.save(korpa);
        }

        return korpa.getStatus();



    }


    @Override
    public Dostavljac findOne(Long id) {
        Dostavljac korisnik = dostavljacRepository.getOne(id);
        return korisnik;
    }

    @Override
    public void brisiKorpu(Long id,Long idK){
        Dostavljac dostavljac=dostavljacRepository.getOne(id);
        Korpa korpa=korpaRepository.getOne(idK);
        List<Korpa> korpas= dostavljac.getBaskets();

        korpas.remove(korpa);
        korpa.setStatus(Status.DOSTAVLJENO);
        korpa.setDeliverer(null);
        korpaRepository.save(korpa);


        dostavljac.setBaskets(korpas);
        dostavljacRepository.save(dostavljac);

    }
}
