package ftn.projekat.VanjaProdavnica.service.impl;

import ftn.projekat.VanjaProdavnica.entity.Artikal;
import ftn.projekat.VanjaProdavnica.repository.ArtikalRepository;
import ftn.projekat.VanjaProdavnica.service.ArtikalService;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class ArtikalServiceImpl implements ArtikalService {

    private final ArtikalRepository artikalRepository;

    public ArtikalServiceImpl(ArtikalRepository artikalRepository) {
        this.artikalRepository = artikalRepository;
    }

    @Override
    public List<Artikal> findAll() {
        List<Artikal> artikli = artikalRepository.findAll();
        return artikli;
    }

    @Override
    public void delete(Long id) {
        Artikal korisnik = artikalRepository.getOne(id);
        artikalRepository.delete(korisnik);
    }

    @Override
    public Artikal create(Artikal artikal) throws Exception {
        if (artikal.getId() != null) {
            throw new Exception("ID must be null!");
        }

        Artikal newArtikal = this.artikalRepository.save(artikal);
        return newArtikal;
    }

    @Override
    public Artikal findOne(Long id) {
        Artikal korisnik = artikalRepository.getOne(id);
        return korisnik;
    }
}
